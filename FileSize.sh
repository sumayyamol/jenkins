#!/bin/bash

# File to consider
FILENAME=/tmp/tcpdump
if test -f "$FILENAME"; then
    echo "$FILENAME exists"
else
    echo "$FILENAME has been deleted"
    echo "THE BUILD IS NOW SUCCESSFUL"
    exit 0
fi

# MAXSIZE is 5 MB
MAXSIZE=4000

# Get file size
FILESIZE=$(stat -c%s "$FILENAME")
# Checkpoint
echo "Size of $FILENAME = $FILESIZE bytes."

# The following doesn't work
if (( $FILESIZE > $MAXSIZE )); then
    echo "/tmp is full"
    exit 1
else
    echo "fine"
fi
